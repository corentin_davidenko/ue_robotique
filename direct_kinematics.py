import math

def leg_dk(theta1, theta2, theta3, alpha = 20.69, beta = 5.06 ,l1 = 51, l2 = 63.7, l3 = 93):
    theta1 = math.radians(theta1)
    theta2 = -math.radians(theta2)
    theta3 = math.radians(theta3)
    alpha  = math.radians(alpha)
    beta   = math.radians(beta)

    planProjection = l1 + (l2 * math.cos(theta2 -alpha) ) + (l3 * (math.cos(theta2 + theta3- math.radians(90)+beta)))

    x = planProjection * math.cos(theta1) 
    y = planProjection * math.sin(theta1)
    z = l2 * math.sin(theta2 - alpha) + l3 * math.sin(theta3 +theta2 - math.radians(90) + beta)

    return [x,y,z]

    
print (leg_dk(0,0,0))
print (leg_dk(90,0,0))
print (leg_dk(180,-30.501,-67.819))
print (leg_dk(0,-30.645,38.501))
