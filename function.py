import math
import itertools
import time
import numpy
import pypot.dynamixel

def leg_dk(theta1, theta2, theta3, alpha = 20.69, beta = 5.06 ,l1 = 51, l2 = 63.7, l3 = 93):
    theta1 = math.radians(theta1)
    theta2 = -math.radians(theta2)
    theta3 = math.radians(theta3)
    alpha  = math.radians(alpha)
    beta   = math.radians(beta)

    planProjection = l1 + (l2 * math.cos(theta2 -alpha) ) + (l3 * (math.cos(theta2 + theta3- math.radians(90)+beta)))

    x = planProjection * math.cos(theta1) 
    y = planProjection * math.sin(theta1)
    z = l2 * math.sin(theta2 - alpha) + l3 * math.sin(theta3 +theta2 - math.radians(90) + beta)

    return [x,y,z]


def al_kashi(a,b,c):
    return math.degrees(math.acos((a*a + b*b - c*c) /( 2*a*b)))

def leg_ik(x, y, z, alpha = 20.69, beta = 5.06 ,l1 = 51, l2 = 63.7, l3 = 93):
    #if (x == 0):
     #   x = math.pow(10, -10)
    theta1 = math.degrees(math.atan2(y,x))
    l_proj = math.sqrt(x*x + y*y)
    d13 = l_proj - l1
    d = math.sqrt(d13*d13 + z*z)
    a = math.degrees(math.atan2(z, d13))
    b = al_kashi(l2,d,l3)
    theta2 = a + b + alpha
    theta3 = al_kashi(l2,l3,d) -180 +90 - beta - alpha
    return [theta1, theta2, theta3]


if __name__ == '__main__':

       # we first open the Dynamixel serial port

    with pypot.dynamixel.DxlIO('/dev/ttyUSB0', baudrate=1000000) as dxl_io:

        # we can scan the motors
        found_ids =  dxl_io.scan()  # this may take several seconds
        print 'Detected:', found_ids

        # we power on the motors
        dxl_io.enable_torque(found_ids)

        current_pos =  dxl_io.get_present_position(found_ids)
        
        # we get the current positions
        print 'Current angles:',current_pos

        print 'Current x y z', leg_dk(current_pos[0], current_pos[1], current_pos[2])

    # we power on the motors
        dxl_io.enable_torque(found_ids)

        # we get the current positions
        print 'Current pos:', dxl_io.get_present_position(found_ids)

       
        for x in range(0, 1000):
            # we create a python dictionnary: {id0 : position0, id1 : position1...}
            pos = dict(zip(found_ids, leg_ik(x, y, z)))
            print 'Cmd:', pos

            # we send these new positions
            dxl_io.set_goal_position(pos)
            time.sleep(0.01)  # we wait for 1s

        # we get the current positions
        print 'New pos:', dxl_io.get_present_position(found_ids)

        # we power off the motors
        dxl_io.disable_torque(found_ids)
        time.sleep(1)  # we wait for 1s
 
