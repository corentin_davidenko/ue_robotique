## Ajout d'un schéma ##
Un schéma au format gif est maintenant disponible. Il permet de voir comment fonctionne la marche d'un insecte en 4 étapes. J'ai commencé à m'en inspirer pour creer une fonction qui fait marcher le robot.

- La position initiale est à revoir, non utilisation de la cinematique inverse
- Bien vérifier les nombres négatif est positifs en fonction de la patte. Pour l'instant :
    - leg1 negatif
    - leg2 positif
    - leg3 positif
    - leg4 positif
    - leg5 negatif
    - leg6 negatif
- Penser à commenter le code, pas tres clair pour l'instant. Meme s'il y a une logique récurrente dans chacune des fonctions "step"
- Faire des tests sur le robot pour ajuster les variables :
    - height : Hauteur de levée de la jambe
    - ampli : en degres, l'amplitude d'un pas du robot
    - speed : vitesse d'execution du robot

## Ajout d'une branche ##
J'ai ajouté une branche nommée "test" pour travailler sans déranger le code de la derniere séance. C'était pas vraiment ce que je voulais faire, mais tant pis