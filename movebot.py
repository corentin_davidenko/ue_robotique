import itertools
import time
import numpy
import pypot.robot
from kinematics import *
from contextlib import closing

def init_motors (robot):
    #init position
    for motor in robot.motors :
        motor.compliant = False
        motor.goto_position(0, 0.5)

    time.sleep(1)
    #position is initialized


def change_height(robot, height, sleep_time = 1):
    angles =  computeIK(118.79, 0.0, height)
    for m in robot.shoulders:
        m.goto_position(angles[0], 5)

    for m in robot.elbows:
        m.goto_position(angles[1], 1)

    for m in robot.wraists:
        m.goto_position(angles[2], 1) 

    time.sleep(sleep_time)


with closing(pypot.robot.from_json("robotConfig.json"))  as robot:

    #we code here
    init_motors(robot)

    while(1 == 1):
        change_height(robot, -130, 0)
        change_height(robot, -90,0)
        
        

    
