#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial


def open_serial(port, baud, timeout):
    ser = serial.Serial(port=port, baudrate=baud, timeout=timeout)
    if ser.isOpen():
        return ser
    else:
        print 'SERIAL ERROR'


def close(ser):
    ser.close()


def write_data(ser, data):
    ser.write(data)


def read_data(ser, size=1):
    return ser.read(size)


def to_hex(val):
    return chr(val)


def decode_data(data):
    res = ''
    for d in data:
        res += hex(ord(d)) + ' '
    return res

def listsum(numList):
    theSum = 0x0
    for i in numList:
        theSum = theSum + i
    return theSum

def send_instruction(id, instruction ,*parameters):
    serial_port = open_serial('/dev/ttyUSB0', 1000000, timeout=0.1)
    data_start = to_hex(0xff)
    data_id = to_hex(id)
    data_length = to_hex(len(parameters)+2)
    data_instruction = to_hex(instruction)
    data_checksum = to_hex(~(id + instruction + len(parameters)+2 + listsum(parameters))%256)
    data = data_start + data_start + data_id + data_length + data_instruction
    for i in parameters:
        data = data + to_hex(i)
    data = data + data_checksum

    print "Envoi : " + decode_data(data)
    write_data(serial_port, data)

    d = read_data(serial_port, 6)
    print "Recu  : " + decode_data(d)
    

    
    

if __name__ == '__main__':
    id = 0xd
    instruction = 0x03
    parameters1 = 0x19
    parameters2 = 0x00
    send_instruction(id, instruction, parameters1, parameters2)
