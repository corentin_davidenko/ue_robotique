import itertools
import time
import numpy
import pypot.dynamixel
from inverse_kinematics import *

if __name__ == '__main__':

    # we first open the Dynamixel serial port
    with pypot.dynamixel.DxlIO('/dev/ttyUSB0', baudrate=1000000) as dxl_io:

        # we can scan the motors
        found_ids = [41,42,43]  # dxl_io.scan()  # this may take several seconds
        print 'Detected:', found_ids

        # we power on the motors
        dxl_io.enable_torque(found_ids)

        # we get the current positions
        print 'Current pos:', dxl_io.get_present_position(found_ids)

        for i in range (0, 10):
        
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,-45,35)))) #a
            time.sleep(0.5)  


            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,-45,0)))) #b
            time.sleep(0.5)  

            #c
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,-45,35))))
            time.sleep(0.5)  

            #d
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,0,35))))
            time.sleep(0.5)  

            #e
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,0,0))))
            time.sleep(0.5)  

            #f
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,0,35))))
            time.sleep(0.5)  

            #g
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,45,35))))
            time.sleep(0.5)

            #h
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,45,0))))
            time.sleep(0.5)

            #i
            dxl_io.set_goal_position( dict(zip(found_ids, leg_ik(170,45,35))))
            time.sleep(0.5)  

        
        # we get the current positions
        print 'New pos:', dxl_io.get_present_position(found_ids)

        # we power off the motors
        dxl_io.disable_torque(found_ids)
        time.sleep(1)  # we wait for 1s
