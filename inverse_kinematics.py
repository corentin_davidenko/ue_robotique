import math

def al_kashi(a,b,c):
    return math.degrees(math.acos((a*a + b*b - c*c) /( 2*a*b)))

def leg_ik(x, y, z, alpha = 20.69, beta = 5.06 ,l1 = 51, l2 = 63.7, l3 = 93, m1=1, m2=1, m3=1):
    #if (x == 0):
     #   x = math.pow(10, -10)
        
    theta1 = math.degrees(math.atan2(y,x))

    l_proj = math.sqrt(x*x + y*y)

    d13 = l_proj - l1
    
    d = math.sqrt(d13*d13 + z*z)

    a = math.degrees(math.atan2(z, d13))

    b = al_kashi(l2,d,l3)

    theta2 =-( a + b + alpha)
    
    theta3 = al_kashi(l2,l3,d) -180 +90 - beta - alpha

    return [theta1, theta2, theta3]


#print leg_ik(203.23, 0.0, -14.30)
